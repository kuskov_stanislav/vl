#include <iostream>
#include <vector>


std::vector<int> f(unsigned long vector_size) {

    std::vector<int> available_vector(vector_size);

    int x=0;
    //заполнить вектор
    for (auto it = available_vector.begin(); it != available_vector.end(); ++it)
    {
        *it=x;
        x++;
    }

    for (auto i = 0; i < available_vector.size();)//до конца вектора
    {
        if (available_vector[i] % 2 == 0){//остаток от деления
            available_vector.erase(available_vector.begin() + i);//удаляем элемент (со сдвигом)
            //i останется прежним
        }
        else
            ++i;//если удаления не было увеличиваем i
    }

    return available_vector;
}

int main() {

    std::vector<int> result_vector = f(15);

    for (auto it : result_vector) {
        std::cout << it << " ";
    }

    return 0;
}
